package edu.missouristate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

	@GetMapping("/")
	public String getIndexPage() {
		return "index";
	}
	
	@ResponseBody
	@GetMapping("/hello")
	public String getHello() {
		return "Hello World";
	}
}

