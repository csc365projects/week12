<jsp:include page="heading.jsp">
<!DOCTYPE html>
<html>
<head><jsp:include page="head.jsp"></head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1>Click Me!</h1>
				<p>Sorry, Internal Error!</p>
				<a href="<%=request.getContextPath()%>/" class="btn btn-primary">
					Home
				</a>
			</div>
		</div>	
	</div>
</body>
</html>